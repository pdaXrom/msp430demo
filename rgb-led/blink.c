//***************************************************************************************
//  MSP430 Blink the LED Demo - Software Toggle P1.0
//
//  Description; Toggle P1.0 by xor'ing P1.0 inside of a software loop.
//  ACLK = n/a, MCLK = SMCLK = default DCO
//
//                MSP430x5xx
//             -----------------
//         /|\|              XIN|-
//          | |                 |
//          --|RST          XOUT|-
//            |                 |
//            |             P1.0|-->LED
//
//  Texas Instruments, Inc
//  July 2013
//***************************************************************************************

#include <msp430.h>

void delay(volatile int i)
{
    do i--;
    while(i != 0);
}

void main(void) {
    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;                   // Disable the GPIO power-on default high-impedance mode
                                            // to activate previously configured port settings
    P3DIR |= 0xE0;                          // Set P3.5, P3.6, P3.7 to output direction

    P3OUT |= 0xE0;

    for(;;) {
        volatile unsigned int i;            // volatile to prevent optimization

        P3OUT ^= 0x20;                      // Toggle P1.0 using exclusive-OR

        delay(40000);

        P3OUT ^= 0x20;
        P3OUT ^= 0x40;

        delay(40000);

        P3OUT ^= 0x40;
        P3OUT ^= 0x80;

        delay(40000);

        P3OUT ^= 0x80;
//        i = 10000;                          // SW Delay
//        do i--;
//        while(i != 0);
    }
}
